﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CS481_HW2
{
    public partial class MainPage : ContentPage
    {
        
        Double savenum = 0;

        String op = "";

        public MainPage()
        {
           
            InitializeComponent();

        }

        private void Num(object sender, EventArgs e)
        {
            

            Label answer = Answer;
            if (answer.Text == "0" || op == "=") {
                answer.Text = (sender as Button).Text;
                op = "";
                    }
            else
            {
                    answer.Text = answer.Text + (sender as Button).Text;

            }
        }
        private void Op(object sender, EventArgs e)
        {
            int len = Answer.Text.Length;
             double answer=0;
            switch ((sender as Button).Text)
            {
                case "/":
                case "*":
                case "+":
                case "-":
                    savenum = Convert.ToDouble(Answer.Text.ToString());
                    Answer.Text = "";
                    op= (sender as Button).Text;
                    break;
                case "=":
                    if (op == "/") {
                        if (Answer.Text.ToString() == "0")
                        {
                            Answer.Text = "Undefined:Div by Zero";
                            op = "=";
                            break;
                        }
                        else
                            answer = savenum / Convert.ToDouble(Answer.Text.ToString());
                     } else if (op == "*") {
                        answer = savenum * Convert.ToDouble(Answer.Text.ToString());
                    }
                    else if (op == "-") {
                        answer = savenum - Convert.ToDouble(Answer.Text.ToString());
                    }
                        else if (op == "+") {
                        answer = savenum + Convert.ToDouble(Answer.Text.ToString());   
                    }
                    op = "=";
                        Answer.Text = answer.ToString();
                    break;
                case "Clr":
                    Answer.Text = "0";
                    op = "";
                    savenum = 0;
                    break;

                default: 
                Answer.Text ="error";
                    break;
            }

        }
    }
}
